<?php

/**
 * @file
 *   Integrates Entity Access API with Rules.
 */

/**
* Implements hook_rules_action_info().
*/
function accessapi_rules_action_info() {
  $defaults = array(
   'parameter' => array(
      'account' => array(
        'type' => 'list<user>',
        'label' => t('Users'),
        'save' => TRUE,
      ),
      'type' => array(
        'type' => 'text',
        'label' => t('Entity type'),
        'save' => TRUE,
      ),
      'eid' => array(
        'type' => 'integer',
        'label' => t('Entity ID'),
        'save' => TRUE,
      ),
      'op' => array(
        'type' => 'text',
        'label' => t('Operation'),
        'save' => TRUE,
      ),
      'access_type' => array(
        'type' => 'integer',
        'label' => t('Access type'),
        'save' => TRUE,
      ),
      'weight' => array(
        'type' => 'integer',
        'label' => t('Weight'),
        'save' => TRUE,
      ),
      'duration' => array(
        'type' => 'integer',
        'label' => t('Duration'),
        'description' => t('The number of seconds for which the access rule is applicable.'),
        'save' => TRUE,
      ),
    ),
    'group' => t('User'),
    'access callback' => TRUE,
  );
  $items['accessapi_set_access'] = $defaults + array(
    'label' => t('Set access rules on an entity'),
    'base' => 'accessapi_rules_action_set_access',
  );
  return $items;
}

function accessapi_rules_action_set_access($accounts, $type, $eid, $op, $access_type, $weight) {
  foreach ($accounts as $account) {
    accessapi_set_access($eid, $type, $op, $account->uid, $access_type, $weight, $duration);
  }
}

function accessapi_rules_action_set_access_form_alter(&$form, &$form_state) {
  $form['access_type']['#type'] = 'select';
  $form['access_type']['#options'] = array(
    ACCESSAPI_ALLOWED => t('Allowed'),
    ACCESSAPI_DISALLOWED => t('Disallowed'),
  );
  $form['access_type']['#default_value'] = ACCESSAPI_ALLOWED;
  $form['weight']['#type'] = 'select';
  $form['weight']['#options'] = drupal_map_assoc(range(-20, 20));
  $form['weight']['#default_value'] = 0;
}

/**
* Implements hook_rules_condition_info().
*/
function accessapi_rules_condition_info() {
  $defaults = array(
   'parameter' => array(
      'account' => array(
        'type' => 'user',
        'label' => t('User'),
        'save' => TRUE,
      ),
      'type' => array(
        'type' => 'text',
        'label' => t('Entity type'),
        'save' => TRUE,
      ),
      'eid' => array(
        'type' => 'integer',
        'label' => t('Entity ID'),
        'save' => TRUE,
      ),
      'op' => array(
        'type' => 'text',
        'label' => t('Operation'),
        'save' => TRUE,
      ),
    ),
    'group' => t('User'),
    'access callback' => TRUE,
  );
  $items['accessapi_has_access'] = $defaults + array(
    'label' => t('User has access to act on an entity'),
    'base' => 'accessapi_rules_action_has_access',
  );
  return $items;
}

function accessapi_rules_action_has_access($account, $type, $eid, $op) {
  return accessapi_has_access($eid, $type, $op, $account->uid);
}
